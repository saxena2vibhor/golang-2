package main

import "fmt"

func fact(a float64) float64 {
	if a == 1 || a == 0 {
		return 1
	}
	return a * fact(a-1)
}

func main() {
	fmt.Println(fact(0))
	fmt.Println(fact(7))
	fmt.Println(fact(25)) // in both int type will not be used so using float.
	fmt.Println(fact(99))
}
