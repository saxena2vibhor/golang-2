package main

import (
    "fmt"
    "time"
)

func main() {
    go hello()
    fmt.Println("Main done!")
	time.Sleep(2 * time.Second)
}

func hello() {
    fmt.Println("Hi!")
    time.Sleep(1 * time.Second)
}