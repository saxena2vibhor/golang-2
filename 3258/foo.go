package main

import "fmt"

type apple int8

var tmp = 5

func main() {
	var msg = "Hello"
	fmt.Println(msg)
	var bar apple
	fmt.Println(bar)
	p := func1()
	fmt.Println(*p)
	*p++
	fmt.Println(*p)
}

func func1() *int {
	var a int = 5
	return &a
}
