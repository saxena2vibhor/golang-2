package main

import "fmt"

func array() {
	arr1 := [9]int{1, 2, 3, 4, 5, 6, 7, 8, 9}
	var arr2 = [9]int{1, 2, 3, 4, 5}

	if(arr1 == arr2){
		fmt.Println("Equal array")
	} else{
		fmt.Println("Not Equal")
	}

	for i:=0;i<len(arr1);i++ {
		fmt.Printf("%d ", arr1[i])
	}
	fmt.Println();
}