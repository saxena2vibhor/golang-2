package main

import "fmt"

func main() {
	slice1 := []int{1, 2, 3, 4}
	fmt.Printf("%d ", slice1)
	fmt.Println()

	slice1 = append(slice1, 5)
	fmt.Print("After append:")
	fmt.Printf("%d ", slice1)

	slice1 = slice1[1:3]
	fmt.Println()
	fmt.Print("After Slice:")
	fmt.Printf("%d ", slice1)
}