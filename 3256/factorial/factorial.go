//Factorial of a number
package main

import "fmt"

func main() {
	var fact = 1
	var slice1 = []int{1}
	for i := 1 ; i <= 10 ; i++ {
		fact = i * fact
		slice1 = append(slice1, fact)
	}

	for i := 1 ; i <= 10 ; i++ {
		fmt.Println(i, "! = ", slice1[i])
	}
}