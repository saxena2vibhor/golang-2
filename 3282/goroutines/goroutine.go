/*
this program is to implement goroutine
*/
package main
import "fmt"

func hello(){
	fmt.Println("Hi")
	time.Sleep(1*time.Second)
}

func main(){
	go hello()
	go hello()
	go hello()
	
	fmt.Println("Main done")
	time.Sleep(2*time.Second)
}