/*
this program is to implement map in Golang
*/

package main
import "fmt"

func main() {

  mp := map[string]string{ "a": 1, "b": 2, "c": 3}
  fmt.Println("initial map: ", mp)

  mp["a"] = 4

  fmt.Println("Updated Map: ", mp)
}