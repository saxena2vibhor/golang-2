package main

import (
	"fmt"
)

func binarySearch(arr []int, x int) int {
	low := 0
	high := len(arr) - 1

	for low <= high {
		mid := (low + high) / 2
		if arr[mid] == x {
			return mid
		} else if arr[mid] < x {
			low = mid + 1
		} else {
			high = mid - 1
		}
	}

	return -1
}

func main() {

	var n, x int
	fmt.Print("Enter the size of the array: ")
	_, err := fmt.Scanln(&n)
	if err != nil {
		panic("Invalid input.")
	}
	arr := make([]int, n)
	for i := 0; i < n; i++ {
		fmt.Printf("Enter element %d: ", i+1)
		_, err := fmt.Scanln(&arr[i])
		if err != nil {
			panic("Invalid input.")
		}
	}

	fmt.Print("Enter the element to search: ")
	_, err = fmt.Scanln(&x)
	if err != nil {
		panic("Invalid input.")
	}

	result := binarySearch(arr, x)
	if result == -1 {
		fmt.Println("Element not found")
	} else {
		fmt.Printf("Element found at index %d\n", result)
	}
}
