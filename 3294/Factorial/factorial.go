package main

import "fmt"

func main() {
	fmt.Println("Enter a number")
	var number int
	var factorial int = 1
	fmt.Scanln(&number)
	if number < 0 {
		fmt.Println("factorial is not possible for negative numbers")
	} else if number == 0 || number == 1 {
		factorial = 1
		fmt.Println("factorial is:", factorial)
	} else {
		for i := number; i > 0; i-- {
			factorial = factorial * i
		}
		fmt.Println("factorial is:", factorial)
	}
}
