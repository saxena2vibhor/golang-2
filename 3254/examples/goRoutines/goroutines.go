// experimenting with goRoutines

package main

import "fmt"

func sum(slice1 []int) int {
	total := 0
	for i := 0; i < len(slice1); i++ {
		total = total + slice1[i]
	}
	return total
}

func main() {

	slice1 := []int{1, 2, 3, 4, 5}

	total := sum(slice1)

	fmt.Printf("The sum of the slice is : %d", total)

}
