package main

import "fmt"

func main() {

    var s []int
    if s == nil {
        fmt.Println("nil!")
    }

    array1 := [5]int{4, 5, 7}
    slice1 := []int{1, 2}

    fmt.Println(array1)
    fmt.Println(slice1)

    slice1 = append(slice1, 6)
    fmt.Println(slice1)
    fmt.Println(slice1[2:4]) //"slice[1:2]"
    fmt.Println(slice1[2:])
    fmt.Println(slice1[:2])
    fmt.Println(cap(slice1))
    fmt.Println(len(slice1))

}