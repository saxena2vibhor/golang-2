package main

const const1 = 53

type apple int8

func main() {
	//var msg = "Hello World!"
	// msg := "golang"
	// fmt.Println(msg)
	// fmt.Println(const1)
	// var bar apple
	// //	var bar int8
	// fmt.Println(bar)
	// p := func1()
	// fmt.Println(*p)
	// *p++
	// fmt.Println(*p)

	// //-------------slice---------------------------
	// array1 := [5]int{4, 5, 6}
	// slice1 := []int{1, 2, 3, 4, 5, 6}

	// fmt.Println(array1)
	// fmt.Println(slice1)

	// slice1 = append(slice1, 6)
	// //_ = append(slice1, 6)
	// fmt.Println(slice1)
	// fmt.Println("slice1[1:2]", slice1[1:2]) // from index 1 upto 2 excluding index 2
	// slice1 = slice1[1:]
	// fmt.Println(slice1)

	//----------map-----------------------------
	// 	map1 := map[string]int{
	// 		"foo": 2,
	// 		"bar": 45,
	// 	}
	// 	fmt.Println(map1)
	// 	map1["Pari"] = 25
	// 	fmt.Println(map1)

	// 	slice_new := []int{20, 30, 60, 50, 40, 30, 10}
	// 	fmt.Printf("The sum of the slice is:")
	// 	fmt.Println(sum(slice_new))

	// }

	// func func1() *int {
	// 	var a int = 5
	// 	return &a
	// }

	// func sum(slice []int) int {
	// 	var ans int
	// 	for _, v := range slice {
	// 		ans += v
	// 	}

	// return ans
}
